package org.but.feec.javafx.exception;

public class DataAccessException extends RuntimeException{
    public DataAccessException(String message, Throwable cause){
        super(message,cause);
    }
}
