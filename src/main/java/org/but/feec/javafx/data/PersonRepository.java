package org.but.feec.javafx.data;

import org.but.feec.javafx.api.PersonBasicView;
import org.but.feec.javafx.config.DataSourceConfig;
import org.but.feec.javafx.exception.DataAccessException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PersonRepository {
    public List<PersonBasicView> getPersonBasicView(){
        try(Connection connection = DataSourceConfig.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT id_person, first_name, surname FROM bds.person " +
                            "ORDER BY id_person ASC");
            ResultSet rs = preparedStatement.executeQuery();) {
            List<PersonBasicView> personBasicViewList = new ArrayList<>();
            while(rs.next()) {
                personBasicViewList.add(mapToPersonBasicView(rs));
            }
            return personBasicViewList;
        } catch (SQLException e){
            throw new DataAccessException("Person basic view could not be loaded",e);
        }
    }

    private PersonBasicView mapToPersonBasicView(ResultSet rs) throws SQLException{
        PersonBasicView pBV = new PersonBasicView();
        pBV.setId(rs.getLong("id_person"));
        pBV.setName(rs.getString("first_name"));
        pBV.setSurname(rs.getString("surname"));
        return pBV;
    }

}
